# Revel mailing module

This module is for sending emails from different email clients.

## Setup

### conf/app.conf  
```ini
module.mailing = gitlab.com/bate-modules/mailing

# Authentication information don't have defaults
# Authentication information 1
mail.1.key = hakuna
mail.1.username = test@test.com
mail.1.password = Ginseng420
mail.1.host = smtp.test.com
mail.1.port = 587

# Authentication inforamtion 2
mail.2.key = matata
...

# Further options including defaults
mail.log = false  # Logs sent mails in console
mail.template.dir = mail # Directory where mail templates are placed (relative to app/views)
mail.template.default = default.html # Default template, if no template is used
```
Different SMTP connections can be declared with mail.1, mail.2 ... Digits have to ce consecutive, so AuthInfo can be registered.


### app/init.go
```go
...
// Init Mailing
revel.OnAppStart(func() {
    mail.InitMailing()
})
...
```

## Usage

Access AuthInfo with 
```go
auth := mail.AuthInfos["key"]
```
... where "key" is predefined  key from ``conf/app.conf``  
Create MailData with 
```go
mailData := mail.NewMailData(...)
```
Edit `mailData.From` if necessary. Otherwise ``.Email`` becomes mail.%d.uername. You can set another template than mail.template.default. There is an predefined header.tpl with email header information you can use.  
In the end just
```go
auth.SendMail(mailData) 
```