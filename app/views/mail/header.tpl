From: {{raw .From.String}}
To: {{raw (join .To.StringSlice ", ")}}{{if .CC}}
Cc: {{raw (join .CC.StringSlice ", ")}}{{end}}{{if .BCC}}
Bcc: {{raw (join .BCC.StringSlice ", ")}}{{end}}
Date: {{raw .StringDate}}
X-Sender: {{raw .From.String}}
X-Priority: 1
Return-Path: {{raw .From.String}}
MIME-Version: 1.0
Content-Transfer-Encoding: 8bit
Content-Type: text/html; charset=utf-8
Subject: {{.Subject}}