package mail

import (
	"bytes"
	"fmt"
	"math"
	"net/smtp"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/revel/revel"
	"github.com/revel/revel/logger"

	"gitlab.com/bate-modules/mailing/app/models"
)

var (
	// AuthInfos holds mail server authentication information with username as key
	AuthInfos map[string]AuthInfo

	mailLog logger.MultiLogger

	noAuthOptions []string = []string{"log", "template"}

	doLog            bool
	templateDirConf  string
	baseTemplateConf string
)

func init() {
	AuthInfos = make(map[string]AuthInfo)

	revel.RegisterModuleInit(func(m *revel.Module) {
		mailLog = m.Log
		revel.TemplateFuncs["join"] = strings.Join
	})
}

// AuthInfo holds mail server information
type AuthInfo struct {
	Username string
	Password string
	SMTPUrl  string
	Port     uint16
	Auth     smtp.Auth
}

// SendMail sends mail with configured email authentication. Param should implement models.MailData. It uses Go's smtp.PlainAuth authentication. Email is send with TLS.
func (a AuthInfo) SendMail(mailData models.MailData) error {
	// Prepare MailData
	if len(mailData.From.Email) == 0 {
		mailData.From.Email = a.Username
	}

	// Render template
	templates, err := revel.MainTemplateLoader.Template(path.Join(templateDirConf, mailData.TemplateName))
	if err != nil {
		mailLog.Errorf("Couldn't parse template %v", mailData.TemplateName)
		return err
	}
	var templateBuf = &bytes.Buffer{}
	templates.Render(templateBuf, mailData)

	// Send mail
	err = smtp.SendMail(a.SMTPUrl+":"+strconv.FormatUint(uint64(a.Port), 10), a.Auth, a.Username, mailData.To.EmailStringSlice(), templateBuf.Bytes())
	if err != nil {
		mailLog.Errorf("Couldn't send mail %v to %v, authentication: %v", mailData.Subject, mailData.To.StringSlice(), a.Username)
		return err
	}

	// Log
	if doLog {
		mailLog.Infof("Send %v from %v to %v", mailData.Subject, mailData.From.String(), mailData.To.StringSlice())
	}
	return nil
}

// InitMailing initializes mailing information
func InitMailing() {
	authKeyMap := extractAuthKeyMap()
	for k := range authKeyMap {
		mail := AuthInfo{}
		var foundUsername bool
		var foundPassword bool
		var foundSMTPAddress bool
		var foundPort bool
		var port int

		mail.Username, foundUsername = revel.Config.String(fmt.Sprintf("mail.%s.username", k))
		mail.Password, foundPassword = revel.Config.String(fmt.Sprintf("mail.%s.password", k))
		mail.SMTPUrl, foundSMTPAddress = revel.Config.String(fmt.Sprintf("mail.%s.host", k))
		port, foundPort = revel.Config.Int(fmt.Sprintf("mail.%s.port", k))
		if port < 0 && port > math.MaxUint16 {
			mailLog.Fatalf("Couldn't parse smtp port %d", port)
		} else {
			mail.Port = uint16(port)
		}

		// Throw error if at least one option is missing
		if !foundUsername || !foundPassword || !foundSMTPAddress || !foundPort {
			mailLog.Fatalf("Missing credentials for mail.%s", k)
		}

		mail.Auth = smtp.PlainAuth("", mail.Username, mail.Password, mail.SMTPUrl)

		AuthInfos[k] = mail
	}

	doLog = revel.Config.BoolDefault("mail.log", false)
	templateDirConf = revel.Config.StringDefault("mail.template.dir", "mail")
	baseTemplateConf = revel.Config.StringDefault("mail.template.default", "default.html")
}

// NewMailData creates MailData with least data
func NewMailData(to models.MailAliases, subject string) models.MailData {
	return models.MailData{To: to, Subject: subject, TemplateName: baseTemplateConf, Date: time.Now()}
}

// extractAuthKeyMap gets keys for only mail authentication
func extractAuthKeyMap() map[string]string {
	// Some helper functions
	// trimMailOption trims r"^mail." from an options string
	trimMailOption := func(option string) string {
		return strings.TrimPrefix(option, "mail.")
	}

	// substrAuthKey subtracts r"\..*" from options string to get auth key
	substrAuthKey := func(option string) (result string) {
		dotIndex := strings.Index(option, ".")
		if dotIndex == -1 {
			result = option
		} else {
			result = option[:dotIndex]
		}
		return
	}

	// isAuthOption excludes that option prefix is in noAuthOptions
	isAuthOption := func(option string) bool {
		for _, noAuthOption := range noAuthOptions {
			if strings.HasPrefix(option, noAuthOption) {
				return false
			}
		}
		return true
	}

	// Happening starts here
	mailOptions := revel.Config.Options("mail")

	// Put auth keys into map for uniqueness
	keyMap := make(map[string]string)
	for _, mailOption := range mailOptions {
		authOption := trimMailOption(mailOption)
		if !isAuthOption(authOption) {
			continue
		}
		authKey := substrAuthKey(authOption)
		keyMap[authKey] = ""
	}

	return keyMap
}
