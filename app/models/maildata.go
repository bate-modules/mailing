package models

import "time"

// Date format
const rfc2822 = "Mon, 02 Jan 2006 15:04:05 -0700"

// MailData is the data passed to a email template
type MailData struct {
	From         MailAlias
	To           MailAliases
	CC           MailAliases
	BCC          MailAliases
	Date         time.Time
	Subject      string
	TemplateName string
	CustomData   interface{}
}

// StringDate returns RFC2822 formatted date
func (m MailData) StringDate() string {
	return m.Date.Format(rfc2822)
}
