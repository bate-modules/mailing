package models

import "fmt"

// MailAlias is a model to hold an email with an alias name.
type MailAlias struct {
	Email string
	Alias string
}

func (e MailAlias) String() string {
	if len(e.Alias) == 0 {
		return e.Email
	}
	return fmt.Sprintf("%v <%v>", e.Alias, e.Email)
}

// MailAliases is a slice of EmailAlias
type MailAliases []MailAlias

// StringSlice returns a slice of string, whereas EmailAlias.String() is executed on each element
func (e MailAliases) StringSlice() (result []string) {
	result = make([]string, len(e))
	for index, value := range e {
		result[index] = value.String()
	}
	return
}

// EmailStringSlice returns slice of string with all emails
func (e MailAliases) EmailStringSlice() (result []string) {
	result = make([]string, len(e))
	for index, value := range e {
		result[index] = value.Email
	}
	return
}
